package com.aplicacionempresarial.backend.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.aplicacionempresarial.backend.models.Empleado;
import com.aplicacionempresarial.backend.repositories.EmpleadoRepository;

@Service
public class EmpleadoService {

	@Autowired
	private EmpleadoRepository empleadoRepository;
	
	public List<Empleado> findAll() {
		return empleadoRepository.findAll(Sort.by(Sort.Direction.ASC, "brm"));
	}
	
	public Optional<Empleado> findById(Long id) {
		return empleadoRepository.findById(id);
	}
	

	public void deleteById(Long id) {
		empleadoRepository.deleteById(id);
	}
	
	public <S extends Empleado> S save(S entity) {
		return empleadoRepository.save(entity);
	}

}

package com.aplicacionempresarial.backend.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aplicacionempresarial.backend.models.Empleado;
import com.aplicacionempresarial.backend.services.EmpleadoService;

@RestController
@RequestMapping("/empleado")
public class EmpleadoRest {

	@Autowired
	private EmpleadoService empleadoService;

	@GetMapping
	private ResponseEntity<List<Empleado>> getAllEmpleados() {
		return ResponseEntity.ok(empleadoService.findAll());
	}
	
	@GetMapping("/{id}")
	private ResponseEntity<Optional<Empleado>> getEmpleadoById(@PathVariable Long id) {
		Optional<Empleado> findEmpleado = empleadoService.findById(id);
		if (findEmpleado.isPresent()) {
			return ResponseEntity.ok(findEmpleado);

		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}

	@PostMapping
	private ResponseEntity<Empleado> saveEmpleado(@RequestBody Empleado empleadoBody) {
		try {
			Empleado empleado = empleadoService.save(empleadoBody);
			return ResponseEntity.created(new URI("/empleado/" + empleado.getId())).body(empleado);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}

	@PutMapping("/{id}")
	private ResponseEntity<Empleado> putEmpleado(@RequestBody Empleado empleadoBody, @PathVariable Long id) {
		Optional<Empleado> findEmpleado = empleadoService.findById(id);
		if (findEmpleado.isPresent()) {
			Empleado empleado = findEmpleado.get();
			empleado.setNombre(empleadoBody.getNombre());
			empleado.setPuesto(empleadoBody.getPuesto());
			empleado.setImagen(empleadoBody.getImagen());
			empleado.setBase64(empleadoBody.getBase64());
			return new ResponseEntity<>(empleadoService.save(empleado), HttpStatus.OK);
		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@DeleteMapping("/{id}")
	private ResponseEntity<Optional<Empleado>> deleteEmpleado(@PathVariable Long id) {
		Optional<Empleado> findEmpleado = empleadoService.findById(id);
		if (findEmpleado.isPresent()) {
			empleadoService.deleteById(id);
			return ResponseEntity.ok(findEmpleado);

		} else
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

	}

}

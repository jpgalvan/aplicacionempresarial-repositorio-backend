package com.aplicacionempresarial.backend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.aplicacionempresarial.backend.models.Empleado;

public interface EmpleadoRepository extends JpaRepository<Empleado, Long>{
	

}
